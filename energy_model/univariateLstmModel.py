#!/usr/bin/python
#
# univariateLstmModel.py
# Joel Wolfrath, 2018
#
# Build a LSTM RNN model for the energy consumption data.
# This model only uses the univariate response to perform prediction
# and ignores the other coviariates used in the study.
#
# Dependencies:
#	tensorflow/keras
#	sklearn
#	matplotlib
#	numpy
#	pandas

from sklearn import preprocessing
from keras import regularizers
import keras.layers as L
import keras.models as M

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
import os.path

#
# Generate some predictors for the univariate series
# This just adds autoregressive terms up to the lag provided
#
def generatePredictors(response, lags):

	dataX, dataY = [], []
	for i in range(len(response)-lags-1):
		a = response[i:(i+lags)]
		dataX.append(a)
		dataY.append(response[i + lags])
	return np.array(dataX), np.array(dataY)
	
def buildLSTMModel(inputShape):

	print('Building LSTM Model...')
	model = M.Sequential()

	model.add(L.LSTM(256, 	dropout=0.0, 
				recurrent_dropout=0.00,
				input_shape=inputShape, 
				return_sequences=True))

	model.add(L.LSTM(64, 	dropout=0.0, 
				recurrent_dropout=0.00,
				input_shape=inputShape))

	model.add(L.Dense(1, activation='sigmoid'))
	model.compile(loss='mean_squared_error', optimizer='adam')
	return model

def r2(y, yhat):
	from sklearn.metrics import r2_score
	return r2_score(y, yhat)

def mse(y, yhat):
	
	s = 0
	for i in range(0, len(y)):
		s += ((y[i] - yhat[i]) ** 2)
	return (s / len(y))

def rmse(y, yhat):
	return mse(y, yhat) ** (0.5)


def undoTransformation(y, scaler):

	y = np.reshape(y, (-1, 1))
	y = scaler.inverse_transform(y)
	return np.squeeze(y)

def printPerformanceMetrics(y, yhat):

	print("\tMSE:  " + str(mse(y,yhat)))
	print("\tRMSE: " + str(rmse(y,yhat)))
	print("\tR^2:  " + str(r2(y,yhat)))

def plotPredictions(y, yhat):

	series_y = pd.Series(y)
	series_yhat = pd.Series(yhat)

	plt.plot(series_y)
	plt.plot(series_yhat)
	plt.show()

def main():

	random.seed(11201992) # November 20, 1992

  INPUT_FILE = '/home/jwolf/coding/stats/energy_model/data/energydata_complete.csv'	
	TRAINING_PERCENT = 0.75
	POLYNOMIAL_DEGREE = 2
	BATCH_SIZE = 128
	NUM_EPOCHS = 16 

	# Read in the data
	huge_data = pd.read_csv(INPUT_FILE, sep=',')
	huge_data = np.array(huge_data["Appliances"])
	
	# Add some autoregresive predictors
	x_raw, y_raw = generatePredictors(huge_data, 12)
	y_raw = np.reshape(y_raw, (-1, 1))

	# Scale the data to the [0,1] range 
	huge_data = np.reshape(huge_data, (-1,1))
	scaler = preprocessing.MinMaxScaler(feature_range=(0,1)).fit(huge_data)
	x_vals = scaler.transform(x_raw)
	y_vals = scaler.transform(y_raw)

	# Uncomment to add polynomial terms
	#print("Adding polynomial terms of degree " + str(POLYNOMIAL_DEGREE))
	#poly = preprocessing.PolynomialFeatures(POLYNOMIAL_DEGREE)
	#x_vals = poly.fit_transform(x_vals)

	SPLIT_INDEX = int(len(y_vals) * TRAINING_PERCENT)
	train_x = x_vals[:SPLIT_INDEX, :]
	test_x = x_vals[SPLIT_INDEX:, :]

	train_x = train_x.reshape((train_x.shape[0], 1, train_x.shape[1]))
	test_x = test_x.reshape((test_x.shape[0], 1, test_x.shape[1]))
	train_y = [y_vals[:SPLIT_INDEX]]
	test_y = [y_vals[SPLIT_INDEX:]]

	model = buildLSTMModel(train_x.shape[1:])

	print('Training...')
	model.fit(train_x, train_y,
	          batch_size=BATCH_SIZE,
	          epochs=NUM_EPOCHS,
	          validation_data=(test_x, test_y))

	predict_train = model.predict(train_x, batch_size = BATCH_SIZE)
	predict_test = model.predict(test_x, batch_size = BATCH_SIZE)

	y_train_actual = undoTransformation(train_y, scaler)
	y_test_actual = undoTransformation(test_y, scaler)
	yhat_train = undoTransformation(predict_train, scaler)
	yhat_test = undoTransformation(predict_test, scaler)

	print "Training data performance:"
	printPerformanceMetrics(y_train_actual, yhat_train)
	print "Test data performance:"
	printPerformanceMetrics(y_test_actual, yhat_test)
	
	plotPredictions(y_test_actual, yhat_test)

if __name__ == "__main__":
	main()


